# CAPACITOR ON VITE-JS

A webapp, mobile app, with ViteJS, ReactJS, and Capacitor 5

## Setting up

Requirements
- Node 18
- Java 17, with ATB certifications added
- Gradle 7
- Android Studio "Flamingo"
- XCode 14.3

Dev set up
```sh
git clone https://gitlab.com/htran2/capavit.git
cd capavit
npm install
npm run dev  # to verify webapp
npm run build  # to build stuff into dist/
```

### Android app
```sh
npx cap add android  # from ./dist into ./android
npx cap open android # open project ./android in Android-Studio
In Android-Studio Preferences, set Gradle to use Java 17
Let Android-Studio build for a while
Add a simulator, with SDK 33
Run (PLAY button) # app appears on phone? Done!
npx cap run android # another way to run (without opening AS)
```

### iOS app
```sh
npx cap add ios
npx cap open ios # open in XCode
In XCode, let it build, 
Create/use a simulator, with iOS 16.4 (not 16.2)
Run with that simulator (PLAY button) # app appears on phone? Done!
```



## MY OWN NOTES (How do I get here)

27-July-2023, I've got:
- Node 18.16.0
- Java 17.0.8, with ATB certifications added
- Gradle 7.6
- Android-Studio "flamingo"


Create an empty NodeJS project
`npm init`

Start an empty ViteJS project
`npm create vite@latest --template react`

Install and use an AntD component (optional)

Install Capacitor
https://capacitorjs.com/docs/getting-started
```sh
npm i @capacitor/core
npm i -D @capacitor/cli
npx cap init

npm i @capacitor/android @capacitor/ios

npm run build  <--- to get stuffs built into ./dist
```


### ANDROID
npx cap add android   <--- copies from ./dist into ./android
npx cap open android  <--- open project ./android in Android-Studio

Let Android-Studio build for a minute.

Add a simulator, with SDK 33

Run (PLAY button) <--- app appears on phone? Done first huddle!
npx cap run android <--- another way to run (without opening AS)

```
Development cycle {
	Change some code
	npm run build
	npx cap sync
	npx cap run android OR npx cap open android
	verify
}
```

### IOS
```sh
npx cap add ios
npx cap open ios <--- open in XCode (v14.3.1)
In XCode, let it build, 
   then create/use a simulator, with iOS 16.4 (not 16.2)
Run with that simulator (PLAY button) <--- app appears on phone? Done!
```


