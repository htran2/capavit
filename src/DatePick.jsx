import { useState } from 'react';
import { DatePicker } from 'antd';

const DatePick = () => {
  const [date, setDate] = useState(null);
  return (
    <div style={{ border: '3px dotted aqua', width: '60%', padding: '0.5rem' }}>
      <span style={{ fontSize: '1.5rem' }}>{`Pick a date `}</span>
      <DatePicker onChange={(d) => setDate(d)} />
      <h3>
        You picked: {date ? date.format('DD-MMM-YYYY') : 'None'}
      </h3>
    </div>
  );
};


export default DatePick