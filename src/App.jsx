import { useState } from 'react'
import capacitor from './assets/capacitor.svg'
import transistor from './assets/transistor.svg'
import './App.scss'
import DatePick from './DatePick'


function App() {
  const [count, setCount] = useState(0)
  
  return (
    <div className="App">
      <h1>{`Capacitor 5`}</h1>
      <DatePick />
      <div style={{ margin: '1rem'}}>
        <img src={capacitor} height={100} />
        <img src={transistor} height={100} />
      </div>
    </div>
  )
}

export default App
